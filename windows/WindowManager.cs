using Godot;

public class WindowManager {
	public string Path;
	public bool Loaded = false;
	private Window loadedWindow;
	public Window Window {
		get {
			if (Loaded) {
				return loadedWindow;
			} else {
				Loaded = true;
				LoadWindow();
				return loadedWindow;
			}
		}
	}

	public WindowManager(string path) {
		Path = "res://windows/" + path;
	}

	public void LoadWindow() {
		loadedWindow = ResourceLoader.Load<PackedScene>(Path).Instantiate<Window>();

		loadedWindow.Unresizable = true;
		// loadedWindow.AlwaysOnTop = true;

		loadedWindow.CloseRequested += () => _OnCloseButtonPressed();
	}

	public void Close() {
		loadedWindow.GetParent<Window>().GrabFocus();
		Loaded = false;
		loadedWindow.QueueFree();
	}

	public void _OnCloseButtonPressed () {
		Close();
	}
}
