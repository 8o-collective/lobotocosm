using Godot;

public partial class MainMenuBase : Node
{
	WindowManager nameWindowManager = new WindowManager("name/NameWindow.tscn");
	WindowManager savesWindowManager = new WindowManager("saves/SavesWindow.tscn");
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		var sysgen = new SystemGenerator();
		var planetgen = new PlanetGenerator();
		var stationgen = new StationGenerator();

		// generate 20 names

		for (int i = 0; i < 20; i++) GD.Print(sysgen.GenerateName());
		GD.Print("----");
		for (int i = 0; i < 20; i++) GD.Print(planetgen.GenerateName());
		GD.Print("----");
		for (int i = 0; i < 20; i++) GD.Print(stationgen.GenerateName());
	}

	public void _OnNewGamePressed() {
		if (!nameWindowManager.Loaded) {
			GetTree().Root.AddChild(nameWindowManager.Window);
		}
	}
	
	public void _OnLoadGamePressed() {
		if (!savesWindowManager.Loaded) {
			GetTree().Root.AddChild(savesWindowManager.Window);
		}
	}
}
