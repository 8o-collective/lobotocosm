using Godot;
using System.Collections.Generic;

public struct Joint
{
    public float Angle;  // Angle of the joint
    public int Length;   // Length of the joint segment
    public Vector2 EndPoint; // Endpoint of the joint segment
    public Line2D Line;  // Visual representation

    public Joint(float angle, int length)
    {
        Angle = angle;
        Length = length;
        EndPoint = new Vector2();
        Line = new Line2D { Width = 1, DefaultColor = Colors.White };
    }
}

public partial class Arm : Node2D
{
    public Vector2 InitialPoint;

    public List<Joint> Joints;

	public float JointAngle = -0.4f;
	public int JointLength = 100;

    public Arm(Vector2 initialPoint, float normalAngle, int jointCount)
    {
        InitialPoint = initialPoint;
        Joints = new List<Joint>();

        // For the sake of demonstration, initialize joints with default values.
        // You can modify this to have diverse angles and lengths if required.
        for (int i = 0; i < jointCount; i++)
        {
            Joint joint = new((i > 0 ? Joints[i - 1].Angle : normalAngle) + JointAngle, JointLength);
            Joints.Add(joint);
            AddChild(joint.Line);
        }


        RecalculatePoints();
    }

    public void RecalculatePoints()
    {
        Vector2 currentPoint = InitialPoint;
        for (int i = 0; i < Joints.Count; i++)
        {
            var joint = Joints[i];
            joint.EndPoint = currentPoint + joint.Length * new Vector2(Mathf.Cos(joint.Angle), Mathf.Sin(joint.Angle));
            joint.Line.Points = new Vector2[] { currentPoint, joint.EndPoint };
            currentPoint = joint.EndPoint;
            Joints[i] = joint;  // Reassign the joint after updating.
        }
    }
}

public partial class Galaxy : Node2D
{
    private Vector2 center = new Vector2(0, 0);
    private float centerRadius = 5;

    private int armsCount = 8;
	private int jointsCount = 4;

    private float rotationAngleDelta = 0.0001f;

    public override void _Ready()
    {
        createArms();
    }

    public void createArms()
    {
        for (int i = 0; i < armsCount; i++)
        {
            var angle = i * 2 * Mathf.Pi / armsCount;
            var armStart = center + (centerRadius * 3) * new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));

            var arm = new Arm(armStart, angle, jointsCount);
            AddChild(arm);
        }
    }

    public override void _Draw()
    {
        DrawCircle(center, centerRadius, Colors.White);
    }

    public override void _Process(double delta)
    {
		Rotation += rotationAngleDelta;
        QueueRedraw();
    }
}
