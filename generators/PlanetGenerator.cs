using Godot;

using System;
using System.IO;
using System.Linq;

using Markov;

public class PlanetGenerator
{
	private static readonly string corpusPath = "res://data/corpi/names.txt";
	private static readonly string suffixPath = "res://data/modifiers/planetSuffixes.txt";
	private static readonly string modelPath = "res://data/models/systems.cfg";

	private string[] corpus = File.ReadAllLines(ProjectSettings.GlobalizePath(corpusPath));
	private string[] suffixes = File.ReadAllLines(ProjectSettings.GlobalizePath(suffixPath));

	private Random random = new();

	private MarkovChain<char> chain = new(2);

	public PlanetGenerator() {
		corpus.ToList().ForEach(word => chain.Add(word, 1));
	}

	public string GenerateName() {
		// chance to choose a random value from suffixes
		string suffix = "";

		if (random.Next(0, 100) < 10) {
			suffix = suffixes[random.Next(0, suffixes.Length)];
		}

		return new string(chain.Chain(random).ToArray()) + suffix;
	}
}
