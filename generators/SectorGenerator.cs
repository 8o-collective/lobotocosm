using Godot;

using System;

using Godot;
using System;
using System.Collections.Generic;

public class PoissonSampler
{
    private Random random = new Random();
    private const int K = 30; // Maximum number of samples before rejection.
    private float cellSize;
    private Vector2 gridSize;
    private Vector2[,] grid;
    private List<Vector2> activeList = new List<Vector2>();
    private List<Vector2> samplePoints = new List<Vector2>();

    public List<Vector2> GeneratePoints(float radius, Vector2 sampleRegionSize, Vector2? start = null)
    {
        cellSize = radius / (float)Math.Sqrt(2);
        gridSize = new Vector2((int)(sampleRegionSize.X / cellSize), (int)(sampleRegionSize.Y / cellSize));
        grid = new Vector2[(int)gridSize.X, (int)gridSize.Y];

        if (start.HasValue)
        {
            activeList.Add(start.Value);
        }
        else
        {
            activeList.Add(sampleRegionSize / 2); // Start from the center.
        }

        while (activeList.Count > 0)
        {
            int activeIndex = random.Next(activeList.Count);
            Vector2 position = activeList[activeIndex];

            bool found = false;
            for (int k = 0; k < K; k++)
            {
                float angle = (float)random.NextDouble() * Mathf.Pi * 2;
                float distance = radius + (float)random.NextDouble() * radius;
                Vector2 sample = position + new Vector2(Mathf.Cos(angle), Mathf.Sin(angle)) * distance;

                if (IsValid(sample, sampleRegionSize, radius))
                {
                    activeList.Add(sample);
                    samplePoints.Add(sample);
                    grid[(int)(sample.X / cellSize), (int)(sample.Y / cellSize)] = sample;
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                activeList.RemoveAt(activeIndex);
            }
        }

        return samplePoints;
    }

    private bool IsValid(Vector2 sample, Vector2 sampleRegionSize, float radius)
    {
        if (sample.X >= 0 && sample.Y >= 0 && sample.X < sampleRegionSize.X && sample.Y < sampleRegionSize.Y)
        {
            int cellX = (int)(sample.X / cellSize);
            int cellY = (int)(sample.Y / cellSize);

            int searchStartX = Math.Max(0, cellX - 2);
            int searchEndX = Math.Min(cellX + 2, (int)gridSize.X - 1);
            int searchStartY = Math.Max(0, cellY - 2);
            int searchEndY = Math.Min(cellY + 2, (int)gridSize.Y - 1);

            for (int x = searchStartX; x <= searchEndX; x++)
            {
                for (int y = searchStartY; y <= searchEndY; y++)
                {
                    Vector2 existingSample = grid[x, y];
                    if (existingSample != Vector2.Zero && existingSample.DistanceTo(sample) < radius)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    }
}

public partial class SectorGenerator
{
	private SystemGenerator sysgen = new();
	private PoissonSampler pointgen = new();

	public SectorGenerator()
	{
		GD.Print(pointgen.GeneratePoints(3, new Vector2(1000, 1000)));
	}
}
