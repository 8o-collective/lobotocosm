using Godot;

using System;
using System.IO;
using System.Linq;

using Markov;

public class StationGenerator
{
	private static readonly string corpusPath = "res://data/corpi/names.txt";
	private static readonly string prefixPath = "res://data/modifiers/stationPrefixes.txt";
	private static readonly string modelPath = "res://data/models/systems.cfg";

	private string[] corpus = File.ReadAllLines(ProjectSettings.GlobalizePath(corpusPath));
	private string[] prefixes = File.ReadAllLines(ProjectSettings.GlobalizePath(prefixPath));

	private Random random = new();

	private MarkovChain<char> chain = new(2);

	public StationGenerator() {
		corpus.ToList().ForEach(word => chain.Add(word, 1));
	}

	public string GenerateName() {
		// random chance to have a prefix
		string prefix = "";

		if (random.Next(0, 100) < 90) {
			prefix = prefixes[random.Next(0, prefixes.Length)] + " ";

			// random chance to be numbers instead of a name
			if (random.Next(0, 100) < 25) {
				return prefix + random.Next(100, 3000).ToString();
			}
		}

		return prefix + new string(chain.Chain(random).ToArray());
	}
}
