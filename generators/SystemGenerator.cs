using Godot;

using System;
using System.IO;
using System.Linq;

using Markov;

public class SystemGenerator
{
	private static string corpusPath = "res://data/corpi/systems.txt";
	private static string modelPath = "res://data/models/systems.cfg";

	private string[] corpus = File.ReadAllLines(ProjectSettings.GlobalizePath(corpusPath));

	private Random random = new();

	private MarkovChain<char> chain = new(2);

	public SystemGenerator() {
		corpus.ToList().ForEach(word => chain.Add(word, 1));
	}

	public string GenerateName() {
		// random chance to have a 3-4 digit number at the end of the name
		string suffix = "";

		if (random.Next(0, 100) < 10) {
			suffix = " " + random.Next(100, 3000).ToString();
		}

		return new string(chain.Chain(random).ToArray()) + suffix;
	}
}
