enum Resources {
	// life support
	Water,
	Ammonia,
	Phosphorus,

	// fuels
	Tritium,
	Deuterium,
	Uranium,
	Thorium,
	Xenon,

	// hydrocarbons
	Methane,
	Ethane,
	Propane,
	
	// rare metals
	Gold,
	Platinum,
	Iridium,
	Palladium,

	// industry metals
	Iron,
	Copper,
	Aluminum,
	Silicon,
	Titanium,
	Lithium,
}

enum AtmosphericResources {
	// life support
	Oxygen,
	Nitrogen,
	CarbonDioxide,
	Argon,
	Neon,
}